package co.vulpin.marv.shard


class NetworkUtils {

    private static final Random random = new Random()

    static boolean getAvailablePort() {

        def port = getRandomPort()

        while(!portIsAvailable(port)) {
            port = getRandomPort()
        }

        return port

    }

    static boolean portIsAvailable(int port) {

        try {
            ServerSocket socket = new ServerSocket(port)
            socket.close()
            println port + " is not in use!"
            return true
        } catch (Exception e) {
            e.printStackTrace()
            println port + " is in use!"
            return false
        }

    }

    private static int getRandomPort() {

        def min = 1024
        def max = Math.pow(2, 16) - Math.pow(2, 14) as int

        def port = random.nextInt(max - min) + min

        return port

    }

}
